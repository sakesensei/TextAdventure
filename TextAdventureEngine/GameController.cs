﻿using System;
namespace TextAdventureEngine
{
	public class GameControlller
	{
		#region Properties
		private Game currentGame;
		public Game CurrentGame
		{
			get { return currentGame; }
			private set { if (value != null) currentGame = value; }
		}

		private GameSystem gSys;
		public GameSystem GSys
		{
			get { return gSys; }
			set { if (value != null) gSys = value; }
		}


		#endregion

		public GameControlller(Game game, GameSystem gameSystem)
		{
			this.CurrentGame = game;
			this.GSys = gameSystem;
		}
	}
}
