﻿using System;
using System.Collections.Generic;

using TextAdventureEngine.GameElements;

namespace TextAdventureEngine
{
	public class GameSystem
	{
		private static Dictionary<string, GameAction> actionSynonim =
			new Dictionary<string, GameAction>{
				{"go", GameAction.go}, {"move", GameAction.go},
				{"quit", GameAction.exit}};

		public static GameAction GetAction(string input)
		{
			actionSynonim.TryGetValue(input, out GameAction key);
			return key;
		}

		private void SplitInput(string rawInput)
		{
			rawInput = rawInput.Trim().ToLower();

			string[] splitInput = rawInput.Split(' ');

			for (int i = 0; i < splitInput.Length; i++)
			{
				if (actionSynonim.TryGetValue(splitInput[0], out GameAction action))
				{

				}
			}
		}

		public GameSystem()
		{
		}
	}
}
