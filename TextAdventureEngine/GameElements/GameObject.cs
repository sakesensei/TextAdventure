﻿using System;
namespace TextAdventureEngine.GameElements
{
	public class GameObject
	{
		#region Properties
		private string name;
		public string Name
		{
			get { return name; }
			set
			{
				if (!String.IsNullOrEmpty(value)) name = value;
				else name = "NONAME";
			}
		}

		private string description;
		public string Description
		{
			get { return description; }
			set
			{
				if (!String.IsNullOrEmpty(value))
				{
					description = value;
				}
				else
				{
					description = $"It's a {Name}.";
				}
			}
		}
		#endregion


		public GameObject(string name, string description)
		{
			this.Name = name;
			this.Description = description;
		}
	}
}
