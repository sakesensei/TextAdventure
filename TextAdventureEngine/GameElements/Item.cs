﻿using System;
namespace TextAdventureEngine.GameElements
{
	public class Item : GameObject
	{
		public Item(string name, string description) : base(name, description)
		{
			this.Name = name;
			this.Description = description;
		}
	}
}
