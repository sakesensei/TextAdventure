﻿using System;
using System.Collections.Generic;

namespace TextAdventureEngine.GameElements
{
	public class Room : GameObject, IContainer
	{
		private List<GameObject> content;
		public List<GameObject> Content
		{
			get { return content; }
			set { content = value; }
		}

		private List<Exit> exits;
		public List<Exit> Exits
		{
			get { return exits; }
			private set { exits = value; }
		}

		public void AddItem(GameObject item)
		{
			if (item != null) Content.Add(item);
		}

		public List<GameObject> GetItems()
		{
			return Content;
		}

		public void RemoveItem(GameObject item)
		{
			if (Content.Contains(item)) Content.Remove(item);
		}

		public Room(string name, string description, List<Exit> exits) : base(name, description)
		{
			this.Name = name;
			this.Description = description;
			this.Exits = exits;
		}
	}
}
