﻿using System;
namespace TextAdventureEngine.GameElements
{
	public class Player : GameObject
	{
		private Inventory playerInventory;
		public Inventory PlayerInventory
		{
			get { return playerInventory; }
			private set { playerInventory = value; }
		}


		public Player(string name, string description) : base(name, description)
		{
			this.Name = name;
			this.Description = description;

			PlayerInventory = new Inventory();
		}
	}
}
