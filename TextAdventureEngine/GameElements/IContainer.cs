﻿using System;
using System.Collections.Generic;

namespace TextAdventureEngine.GameElements
{
	public interface IContainer
	{
		List<GameObject> Content
		{
			get;
			set;
		}

		List<GameObject> GetItems();

		void AddItem(GameObject item);

		void RemoveItem(GameObject item);
	}
}
