﻿using System;
namespace TextAdventureEngine.GameElements
{
	public enum Exit
	{
		north,
		south,
		east,
		west,
		up,
		down
	}
}
