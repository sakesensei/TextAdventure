﻿using System;
namespace TextAdventureEngine.GameElements
{
	public class Door : GameObject, ILock
	{
		public GameObject Unlocker
		{
			get;
			set;
		}
		public bool IsLocked
		{
			get; set;
		}

		public void Lock(GameObject key)
		{
			if (key.Equals(Unlocker) && !IsLocked)
			{
				IsLocked = true;
				// Lock Message
			}
			else
			{
				// Fail
			}
		}

		public void Unlock(GameObject key)
		{
			if (key.Equals(Unlocker) && IsLocked)
			{
				IsLocked = false;
				// Unlock Message
			}
			else
			{
				// Fail
			}
		}

		public Door(string name, string description, GameObject unlocker) : base(name, description)
		{
			this.Name = name;
			this.Description = description;
			this.Unlocker = unlocker;
		}
	}
}
