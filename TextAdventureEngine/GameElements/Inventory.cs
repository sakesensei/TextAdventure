﻿using System;
using System.Collections.Generic;

namespace TextAdventureEngine.GameElements
{
	public class Inventory : IContainer
	{
		private List<GameObject> content;
		public List<GameObject> Content
		{
			get { return content; }
			set { content = value; }
		}

		public void AddItem(GameObject item)
		{
			Content.Add(item);
		}

		public List<GameObject> GetItems()
		{
			return Content;
		}

		// NOTE: this might not work at all
		public void RemoveItem(GameObject item)
		{
			if (Content.Contains(item))
			{
				Content.Remove(item);
			}
		}
	}
}
