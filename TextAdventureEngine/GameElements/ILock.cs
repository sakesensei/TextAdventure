﻿using System;
namespace TextAdventureEngine.GameElements
{
	public interface ILock
	{
		GameObject Unlocker
		{
			get;
			set;
		}

		bool IsLocked
		{
			get;
			set;
		}

		void Unlock(GameObject key);

		void Lock(GameObject key);
	}
}
